// @flow
import pool from './mysql-pool';

export type User = {
  googleID: number,
  name: string,
  balance: number,
  pictureID: string,
};

export type Bet = {
  accumID: Number,
  name: String,
  status: Boolean,
  category: String,
  option_text: String,
  wentIn: Boolean,
  odds: Number,
};
export type Accum = {
  accumID: Number,
  totalOdds: Number,
  stake: Number,
};
export type ABet = {
  id: Number,
  theBet: string,
  category: string,
};
export type Option = {
  optionID: Number,
  valg: string,
  odds: Number,
  keyToBetID: Number,
};

class BetService {
 
  getBets(googleID: number) {
    return new Promise<Bet[]>((resolve, reject) => {
      pool.query(
        'select accumulator_bet.accumID, (select bet.name from bet where accumulator_bet.betID = bet.betID) AS theBet, (select bet.status from bet where accumulator_bet.betID = bet.betID) AS statusBet, (select bet.category from bet where accumulator_bet.betID = bet.betID) AS category, (select bet_option.option_text from bet_option where bet_option.optionID = accumulator_bet.choice) AS valg, (select bet_option.wentIn from bet_option where bet_option.optionID = accumulator_bet.choice) AS wentIn, (select bet_option.odds from bet_option where bet_option.optionID = accumulator_bet.choice) AS odds from accumulator_bet where accumulator_bet.accumID IN (SELECT accumulator.accumID from accumulator WHERE accumulator.googleID = ?)',
        [googleID],
        (error, results) => {
          if (error) return reject(error);
          console.log(googleID);
          console.log(results);
          resolve(results);
        }
      );
    });
  }

  getAllUsers() {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select user.googleID from user',

        (error, results) => {
          if (error) return reject(error);
          resolve(results);
        }
      );
    });
  }
 
  getAllSorted() {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select accumulator_bet.accumID, (select accumulator.googleID from accumulator where accumulator.accumID = accumulator_bet.accumID) as googleID, (select bet.status from bet where accumulator_bet.betID = bet.betID) AS statusBet, (select bet_option.wentIn from bet_option where bet_option.optionID = accumulator_bet.choice) AS wentIn, (select accumulator.totalOdds from accumulator where accumulator.accumID=accumulator_bet.accumID) as totalOdds, (select accumulator.stake from accumulator where accumulator.accumID=accumulator_bet.accumID) as stake, (select accumulator.paidOut from accumulator where accumulator.accumID = accumulator_bet.accumID)as paidOut from accumulator_bet where (select accumulator.settled from accumulator where accumulator.accumID = accumulator_bet.accumID)=0 order by accumID asc;',

        (error, results) => {
          if (error) return reject(error);
          resolve(results);
        }
      );
    });
  }
 
  getAllUsersInfo() {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select * from user order by balance desc',

        (error, results) => {
          if (error) return reject(error);
          resolve(results);
        }
      );
    });
  }

  getOneUser(googleID: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query('select * from user where user.googleID = ?', [googleID], (error, results) => {
        if (error) return reject(error);
        resolve(results[0]);
      });
    });
  }

 
  getAccums(googleID: number) {
    return new Promise<Accum[]>((resolve, reject) => {
      pool.query(
        'select accumulator.accumID, accumulator.totalOdds, accumulator.stake, accumulator.public from accumulator where accumulator.googleID = ? order by accumID desc',
        [googleID],
        (error, results) => {
          if (error) return reject(error);
          console.log('er i get accums');
          console.log(results);
          resolve(results);
        }
      );
    });
  }


  getBalance(googleID: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select user.balance, user.whitelist from user where user.googleID = ?',
        [googleID],
        (error, results) => {
          if (error) return reject(error);
          console.log('er i get balance');
          console.log(results[0]);
          resolve(results[0]);
        }
      );
    });
  }

  getAllBets() {
    return new Promise<ABet[]>((resolve, reject) => {
      pool.query(
        'select bet.betID, bet.name, bet.category from bet where bet.status = 0 order by betID desc',
        (error, results) => {
          if (error) return reject(error);
          console.log('er i getAllBets');
          console.log(results);
          resolve(results);
        }
      );
    });
  }
 
  getOrdbok() {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select * from ordbok where godkjent = 1 order by hyppighet desc',
        (error, results) => {
          if (error) return reject(error);
          resolve(results);
        }
      );
    });
  }
  
  getOneBet(betID: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query('select * from bet where bet.betID = ?', [betID], (error, results) => {
        if (error) return reject(error);
        console.log('er i getOneBet');
        console.log([0]);
        resolve(results[0]);
      });
    });
  }
 
  getOptions(betID: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'select * from bet_option where bet_option.betID = ?',
        [betID],
        (error, results) => {
          if (error) return reject(error);
          console.log('er i getOneBet');
          console.log(results);
          resolve(results);
        }
      );
    });
  }
  
  getDefAllBets() {
    return new Promise<any>((resolve, reject) => {
      pool.query('select * from bet', (error, results) => {
        if (error) return reject(error);
        resolve(results);
        console.log(results);
      });
    });
  }
 
    getDefAllBetsReq() {
      return new Promise<any>((resolve, reject) => {
        pool.query('select * from request', (error, results) => {
          if (error) return reject(error);
          resolve(results);
          console.log(results);
        });
      });
    }
  
       getAllReqOpts() {
        return new Promise<any>((resolve, reject) => {
          pool.query('select * from req_option', (error, results) => {
            if (error) return reject(error);
            resolve(results);
            console.log(results);
          });
        });
      }
  
  getAllBetOptions() {
    return new Promise<Option[]>((resolve, reject) => {
      pool.query(
        'select bet_option.optionID, bet_option.option_text AS valg, bet_option.odds, bet_option.betID AS keyToBetID from bet_option where bet_option.betID in (select bet.betID from bet where bet.status = 0)',
        (error, results) => {
          if (error) return reject(error);
          console.log('er i getAllBetOptions');
          console.log(results);
          resolve(results);
        }
      );
    });
  }

  makeAccum(googleID: number, totalOdds: number, innsats: number) {
    console.log("Inni service: ", googleID, totalOdds, innsats)
    return new Promise<number>((resolve, reject) => {
      pool.query(
        'insert into accumulator (googleID, totalOdds, stake) VALUES (?, ?, ?)',
        [googleID, totalOdds, innsats],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
          console.log("ER HER!!! " + results.insertId);
        }
      );
    });
  }

  placeBet(accumID: number, betID: number, optionID: number) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into accumulator_bet (accumID, betID, choice) VALUES (?, ?, ?)',
        [accumID, betID, optionID],
        (error, results) => {
          if (error) return reject(error);
          resolve(results.insertId);
        }
      );
    });
  }

  sendForslag(ordet: string, beskrivelse: string, hyppighet: number, innsender: string) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into ordbok (ordet, beskrivelse, hyppighet, innsender, godkjent) VALUES (?, ?, ?, ?, ?)',
        [ordet, beskrivelse, hyppighet, innsender, 1],
        (error, results) => {
          if (error) return reject(error);
          resolve(results.insertId);
          console.log('sendt ', results.insertId);
        }
      );
    });
  }

  addUser(googleID: string, googleName: string) {
    console.log(googleID, googleName);
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into user (googleID, name, balance) values (?, ?, ?)',
        [googleID, googleName, 2000],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
          console.log(results.insertId);
        }
      );
    });
  }
  
  sendBet(theBet: string, category: string) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into bet (status, name, category) values (?, ?, ?)',
        [0, theBet, category],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
          console.log(results.insertId);
        }
      );
    });
  }

  sendReq(theBet: string, category: string, name: string) {
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into request (status, name, category, requestedBy) values (?, ?, ?, ?)',
        [0, theBet, category, name],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
          console.log(results.insertId);
        }
      );
    });
  }

  sendAlt(option_text: string, odds: Number, betID: Number) {
    console.log(option_text, odds, betID);
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into bet_option (option_text, wentIn, odds, betID) values (?, ?, ?, ?)',
        [option_text, 0, odds, betID],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
        }
      );
    });
  }

  sendReqAlt(option_text: string, odds: Number, betID: Number) {
    console.log(option_text, odds, betID);
    return new Promise<any>((resolve, reject) => {
      pool.query(
        'insert into req_option (option_text, wentIn, odds, betID) values (?, ?, ?, ?)',
        [option_text, 0, odds, betID],
        (error, results) => {
          if (error) return reject(error);
          resolve(Number(results.insertId));
        }
      );
    });
  }

  updateBalance(googleID: string, innsats: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.balance=? WHERE user.googleID=?',
        [innsats, googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  changePublic(accumID: number, offentlig: boolean) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE accumulator SET accumulator.public=? WHERE accumulator.accumID=?',
        [offentlig, accumID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  incWaitAccums(googleID: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.waitingAccums=user.waitingAccums+1 WHERE user.googleID=?',
        [googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  payOut(amount: number, googleID: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.balance=user.balance+? WHERE user.googleID=?',
        [amount, googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  setPaidOut(setValue: number, accumID: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE accumulator SET accumulator.paidOut=?, accumulator.settled=? WHERE accumulator.accumID=?',
        [setValue, setValue, accumID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  setSettled(setValue: number, accumID: string) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE accumulator SET accumulator.settled=? WHERE accumulator.accumID=?',
        [setValue, accumID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  editBetText(newText: string, betID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('UPDATE bet SET bet.name=? WHERE bet.betID=?', [newText, betID], (error) => {
        if (error) return reject(error);
        //if (!results.insertId) return reject(new Error('No row inserted'));

        resolve();
      });
    });
  }

  editBetStatus(status: number, betID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query('UPDATE bet SET bet.status=? WHERE bet.betID=?', [status, betID], (error) => {
        if (error) return reject(error);
        //if (!results.insertId) return reject(new Error('No row inserted'));

        resolve();
      });
    });
  }

  editOptionStatus(status: number, optionID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE bet_option SET bet_option.wentIn=? WHERE bet_option.optionID=?',
        [status, optionID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  editOptionText(newText: string, odds: Number, optionID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE bet_option SET bet_option.option_text=?, bet_option.odds=? WHERE bet_option.optionID=?',
        [newText, odds, optionID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  noWaiting(googleID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.waitingAccums=0 WHERE user.googleID=?',
        [googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  loginStamp(googleID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.lastLogin=current_timestamp WHERE user.googleID=?',
        [googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }

  changeValue(newValue: number, googleID: number) {
    return new Promise<void>((resolve, reject) => {
      pool.query(
        'UPDATE user SET user.whitelist=? WHERE user.googleID=?',
        [newValue, googleID],
        (error) => {
          if (error) return reject(error);
          //if (!results.insertId) return reject(new Error('No row inserted'));

          resolve();
        }
      );
    });
  }
  
const betService = new BetService();
export default betService;
