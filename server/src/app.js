// @flow

import express from 'express';
import betRouter from './bet-router';

/**
 * Express application.
 */
const app: express$Application<> = express();

app.use(express.json());

app.use('/api/v1', betRouter);

export default app;
