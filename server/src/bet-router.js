// @flow
import express from 'express';
import betService from './bet-service';

//Express router containing task methods.

const router: express$Router<> = express.Router();



//get all bets for a spesific user
router.get('/profile/getBets/:googleID', (request, response) => {
  const googleID = request.params.googleID;
  console.log('inni getBets: ' + googleID);
  betService
    .getBets(googleID)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//get all accums for a spesific user
router.get('/profile/getAccums/:googleID', (request, response) => {
  const googleID = request.params.googleID;
  console.log(googleID);
  betService
    .getAccums(googleID)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//get balance for a spesific user
router.get('/profile/getBalance/:googleID', (request, response) => {
  const googleID = request.params.googleID;
  console.log(googleID);
  betService
    .getBalance(googleID)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//task method for getting all non settled bets
router.get('/odds/getAll', (request, response) => {
  betService
    .getAllBets()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//task method for getting all bets
router.get('/admin/getAllBets', (request, response) => {
  betService
    .getDefAllBets()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting all bet requests
router.get('/admin/getAllReqs', (request, response) => {
  betService
    .getDefAllBetsReq()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting all bet request options
router.get('/admin/getAllReqOpts', (request, response) => {
  betService
    .getAllReqOpts()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting one bet by id
router.get('/admin/getOneBet/:betID', (request, response) => {
  const oneBetID = request.params.betID;
  betService
    .getOneBet(oneBetID)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting all options for one bet by id
router.get('/admin/getOptions/:betID', (request, response) => {
  const oneBetIDoptions = request.params.betID;
  betService
    .getOptions(oneBetIDoptions)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//get all bet options for non settled bets
router.get('/odds/getAllOptions', (request, response) => {
  betService
    .getAllBetOptions()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//task method for getting all users 
router.get('/home/allUsers', (request, response) => {
  betService
    .getAllUsers()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting all users with additional admin info
router.get('/admin/allUsers', (request, response) => {
  betService
    .getAllUsersInfo()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//task method for getting info about one user by googleID
router.get('/admin/oneUser/:googleID', (request, response) => {
  const googleID = request.params.googleID;
  betService
    .getOneUser(googleID)
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});
//complicated query for getting all accumulators made by users
router.get('/admin/getAllSorted', (request, response) => {
  betService
    .getAllSorted()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//task method for getting all words in dictionary
router.get('/ordbok/getHele', (request, response) => {
  betService
    .getOrdbok()
    .then((rows) => response.send(rows))
    .catch((error: Error) => response.status(500).send(error));
});

//post an accumulator
router.post('/odds/makeAccum', (request, response) => {
  const data = request.body;
  console.log(data.googleID, data.totalOdds, data.innsats)
  betService
    .makeAccum(data.googleID, data.totalOdds, data.innsats)
    .then((accumID) => response.send({ accumID: accumID }))
    .catch((error: Error) => response.status(500).send(error));
});

//connect all bets in an accumulator to a user
router.post('/odds/placeBet', (request, response) => {
  const data = request.body;
  betService
    .placeBet(data.accumID, data.betID, data.optionID)
    .then((accumID) => response.send({ accumID: accumID }))
    .catch((error: Error) => response.status(500).send(error));
});

//change an accumulator between private and public
router.put('/profile/changePublic', (request, response) => {
  const data = request.body;
  betService
    .changePublic(data.accumID, data.offentlig)
    .then((accumID) => response.send({ accumID: accumID }))
    .catch((error: Error) => response.status(500).send(error));
});

//add a new user
router.post('/home/addUser', (request, response) => {
  const data = request.body;
  console.log(data);
  betService
    .addUser(data.googleID, data.googleName)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//send word request
router.put('/ordbok/sendForslag', (request, response) => {
  const data = request.body;
  betService
    .sendForslag(data.ordet, data.beskrivelse, data.hyppighet, data.innsender)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//update balance for user
router.put('/odds/updateBalance', (request, response) => {
  const data = request.body;
  betService
    .updateBalance(data.googleID, data.innsats)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//increase num of waiting accums for a user
router.put('/admin/incWaitAccums', (request, response) => {
  const data = request.body;
  betService
    .incWaitAccums(data.googleID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//edit a bet text (admin)
router.put('/admin/editBetText', (request, response) => {
  const data = request.body;
  console.log(data);
  console.log(data.newText);
  betService
    .editBetText(data.newText, data.betID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//change a bet from open to settled
router.put('/admin/editBetStatus', (request, response) => {
  const data = request.body;
  betService
    .editBetStatus(data.status, data.betID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//pay out winnings to a user
router.put('/admin/payOut', (request, response) => {
  const data = request.body;
  betService
    .payOut(data.amount, data.googleID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//set an accumulator to paid out
router.put('/admin/setPaidOut', (request, response) => {
  const data = request.body;
  betService
    .setPaidOut(data.setValue, data.accumID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//set an accumulator to settled
router.put('/admin/setSettled', (request, response) => {
  const data = request.body;
  betService
    .setSettled(data.setValue, data.accumID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//edit an option in a bet to success or failed
router.put('/admin/editOptionStatus', (request, response) => {
  const data = request.body;
  betService
    .editOptionStatus(data.status, data.optionID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//edit text in a bet option
router.put('/admin/editOptionText', (request, response) => {
  const data = request.body;
  /*  console.log(data);
  console.log(data.newText); */
  betService
    .editOptionText(data.newText, data.odds, data.optionID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//clear out all waiting accums for a user that logs in
router.put('/home/noWaiting', (request, response) => {
  const data = request.body;
  betService
    .noWaiting(data.googleID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//login stamp to see when a user last logged in
router.put('/home/loginStamp', (request, response) => {
  const data = request.body;
  betService
    .loginStamp(data.googleID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//add or remove a user from whitelist (admin task)
router.put('/admin/changeRights/', (request, response) => {
  const data = request.body;
  betService
    .changeValue(data.newValue, data.googleID)
    .then((id) => response.send({ id: id }))
    .catch((error: Error) => response.status(500).send(error));
});

//send a bet to database (admin)
router.post('/admin/sendBet', (request, response) => {
  const data = request.body;
  betService
    .sendBet(data.theBet, data.category)
    .then((betID) => response.send({ betID: betID }))
    .catch((error: Error) => response.status(500).send(error));
});

//send a bet request
router.post('/admin/sendReq', (request, response) => {
  const data = request.body;
  betService
    .sendReq(data.theBet, data.category, data.name)
    .then((betID) => response.send({ betID: betID }))
    .catch((error: Error) => response.status(500).send(error));
});

//send a bet option (admin)
router.post('/admin/sendAlt', (request, response) => {
  const data = request.body;
  betService
    .sendAlt(data.option_text, data.odds, data.betID)
    .then((betID) => response.send({ betID: betID }))
    .catch((error: Error) => response.status(500).send(error));
});

//send a requested bet option
router.post('/admin/sendReqAlt', (request, response) => {
  const data = request.body;
  betService
    .sendReqAlt(data.option_text, data.odds, data.betID)
    .then((betID) => response.send({ betID: betID }))
    .catch((error: Error) => response.status(500).send(error));
});

export default router;
