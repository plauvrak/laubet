// @flow

import app from './app';
import express from 'express';
import path from 'path';
import http from 'http';

// Serve client files
app.use(express.static(path.join(__dirname, '/../../client/public')));

const port = 10443;
app.listen(port, () => {
  console.info(`Server running on port ${port}`);
});

