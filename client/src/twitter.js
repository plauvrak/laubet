// @flow
import * as React from 'react';
import { Component } from 'react-simplified';
import betService, { type Question } from './quiz-service';
import { Card, Row, Column, Form, Button, NavBar, CenterCard } from './widgets';
import { createHashHistory } from 'history';
import { TwitterTweetEmbed } from 'react-twitter-embed';

const history = createHashHistory();

export class Twitter extends Component {
  render() {
    return (
      <>
        <Card>
          <div className="CenterDiv">
          <TwitterTweetEmbed tweetId={'1396143266689597443'} />
            <br></br>
          <TwitterTweetEmbed tweetId={'1462333987393871874'} />
            <br></br>
          <TwitterTweetEmbed tweetId={'1454184259900542992'} />
            <br></br>
          <TwitterTweetEmbed tweetId={'1403815073492127746'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'1332286824279060482'} />
            <br></br>

            <TwitterTweetEmbed tweetId={'699694408552312832'} />

            <br></br>
            <TwitterTweetEmbed tweetId={'1314308417809457189'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'1327617292310614016'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'868365597482119169'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'1328832438445936640'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'1258147754829905921'} />
            <br></br>
            <TwitterTweetEmbed tweetId={'912589927652773889'} />
          </div>
        </Card>
      </>
    );
  }
}
