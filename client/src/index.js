// @flow
import ReactDOM from "react-dom";
import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import { NavBar, Card, Accum, Alert } from "./widgets";
import { NavLink, HashRouter, Route } from "react-router-dom";

//komponenter
import { Home } from "./home.js";

import { OddsOversikt } from "./oddsoversikt.js";
import { Twitter } from "./twitter.js";
import { Turnering } from "./turnering.js";
import { LoginTest } from "./logintest.js";
import { OddsNy } from "./oddsny";
import { BetSlip } from "./oddsny";
import { RequestBet } from "./request";
import { Leaderboard } from "./leaderboard";
import { Admin } from "./admin";
import { OtherUser } from "./otherUser";
import { EditBet } from "./editBet";
import { OrdbokNy } from "./ordbokny";
import { LookOtherUser } from "./lookotheruser";

//toppmeny øverst på siden vår
class Menu extends Component {
  render() {
    return (
      <>
        <div class="dropdown">
          <div class="burger">
            <img height="28" src="/Media/burger.png" />
          </div>
          <div class="dropdown-content">
            <NavBar>
              <NavBar.Link to="/">
                <img height="30" src="/Media/logo3.png" />
              </NavBar.Link>
              <NavBar.Link to="/logintest">Min side</NavBar.Link>
              <NavBar.Link to="/oddsny">Odds</NavBar.Link>
              <NavBar.Link to="/ordbokNy">Landvik-ordboka</NavBar.Link>
              <NavBar.Link to="/twitter">Twitter</NavBar.Link>
              <NavBar.Link to="/leaderboard">Leaderboard</NavBar.Link>
              <NavBar.Link to="/request">Request-a-bet</NavBar.Link>
              <NavBar.Link to="/admin">Admin</NavBar.Link>
            </NavBar>
          </div>
        </div>

      </>
    );
  }
}

const root = document.getElementById("root");
if (root)
  ReactDOM.render(
    <>
      <HashRouter>
        <div>
          <Menu />
          <Alert />
          <Route exact path="/" component={Home} />
          <Route path="/odds" component={OddsOversikt} />
          <Route exact path="/ordbokNy" component={OrdbokNy} />
          <Route exact path="/twitter" component={Twitter} />
          <Route exact path="/oddsNy" component={OddsNy} />
          <Route exact path="/logintest" component={LoginTest} />
          <Route exact path="/leaderboard" component={Leaderboard} />
          <Route exact path="/request" component={RequestBet} />
          <Route exact path="/admin" component={Admin} />
          <Route exact path="/admin/bets/:googleID" component={OtherUser} />
          <Route exact path="/admin/:betID" component={EditBet} />
          <Route
            exact
            path="/leaderboard/:googleID/:name"
            component={LookOtherUser}
          />
          <BetSlip />
        </div>
      </HashRouter>
    </>,
    root
  );
