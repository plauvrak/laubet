import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./bet-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";
import { type UserAccum, type ABet, type Option } from "./oddsny.js";

export class EditBet extends Component<{
  match: { params: { betID: number } },
}> {
  bet = { betID: 0, status: 0, name: "", category: "" };
  betOptions: Option[] = [];
  betID = this.props.match.params.betID;
  betDisabled = true;
  optionsDisabled = true;
  newOptions: [] = [];
  changeStatus = false;
  allBets: [] = [];
  sorted: [] = [];
  accumWentIn = false;
  accumHasLost = false;
  isAccumFinished = true;

  render() {
    if (1 == 1) {
      return (
        <>
          <Card>
            <Row>
              <Column>
                <Form.Input
                  disabled={this.betDisabled}
                  value={this.bet.name}
                  onChange={(event) => {
                    this.bet.name = event.currentTarget.value;
                  }}
                ></Form.Input>
              </Column>
              <Column width={"10%"} right>
                {this.betDisabled ? (
                  <Button.Light
                    onClick={() => {
                      if (this.betDisabled == true) {
                        this.betDisabled = false;
                      } else {
                        this.betDisabled = true;
                      }
                    }}
                  >
                    Endre
                  </Button.Light>
                ) : (
                  <Button.Success
                    onClick={() => {
                      this.betDisabled = true;
                      betService.editBetText(this.bet.name, this.bet.betID);
                    }}
                  >
                    Lagre
                  </Button.Success>
                )}
              </Column>
            </Row>
            <br></br>
            {this.betOptions.map((option) => {
              return (
                <>
                  <Row>
                    <Column>
                      <Form.Input
                        disabled={this.optionsDisabled}
                        value={option.option_text}
                        onChange={(event) => {
                          option.option_text = event.currentTarget.value;
                        }}
                      ></Form.Input>
                    </Column>
                    <Column right width={"20%"}>
                      <Form.Input
                        disabled={this.optionsDisabled}
                        value={option.odds}
                        onChange={(event) => {
                          option.odds = event.currentTarget.value;
                        }}
                      ></Form.Input>
                    </Column>
                  </Row>
                </>
              );
            })}
            {this.newOptions.map((option) => {
              return (
                <>
                  <Row>
                    <Column>
                      <Form.Input
                        disabled={this.optionsDisabled}
                        value={option.option_text}
                        onChange={(event) => {
                          option.option_text = event.currentTarget.value;
                        }}
                      ></Form.Input>
                    </Column>
                    <Column right width={"20%"}>
                      <Form.Input
                        disabled={this.optionsDisabled}
                        value={option.odds}
                        onChange={(event) => {
                          option.odds = event.currentTarget.value;
                        }}
                      ></Form.Input>
                    </Column>
                  </Row>
                </>
              );
            })}
            {this.optionsDisabled ? (
              <Button.Light
                onClick={() => {
                  this.optionsDisabled = false;
                }}
              >
                Endre alternativer
              </Button.Light>
            ) : (
              <Button.Success
                onClick={() => {
                  this.optionsDisabled = true;
                  this.betOptions.forEach((e) => {
                    betService.editOptionText(
                      e.option_text,
                      e.odds,
                      e.optionID
                    );
                  });
                  this.newOptions.forEach((e) => {
                    betService.sendAlts(e.option_text, e.odds, e.betID);
                  });
                  this.newOptions = [];
                  betService
                    .getOptions(this.betID)
                    .then((allOptions) => {
                      (this.betOptions = allOptions), console.log(allOptions);
                    })
                    .catch((error: Error) =>
                      console.error(
                        "Error getting all options: " + error.message
                      )
                    );
                }}
              >
                Lagre
              </Button.Success>
            )}{" "}
            <Button.Light
              onClick={() => {
                this.newOptions.push({
                  option_text: "",
                  odds: null,
                  betID: this.betID,
                });
              }}
            >
              Legg til alternativ
            </Button.Light>
          </Card>
          <Card>
            <Button.Lau
              hidden={this.changeStatus}
              onClick={() => {
                if (this.changeStatus == false) {
                  this.changeStatus = true;
                } else {
                  this.changeStatus = false;
                }
              }}
            >
              {this.changeStatus ? "Avbryt" : "Legg til resultat!"}
            </Button.Lau>
          </Card>
          {this.changeStatus ? (
            <Card title={this.bet.name}>
              {this.betOptions.map((option) => {
                return (
                  <>
                    <Form.Checkbox
                      checked={option.wentIn}
                      onChange={() => {
                        if (option.wentIn) {
                          option.wentIn = 0;
                        } else {
                          option.wentIn = 1;
                        }
                      }}
                    ></Form.Checkbox>{" "}
                    {option.option_text}
                  </>
                );
              })}
              <br></br>
              <br></br>
              <Button.Success onClick={this.updateAllAccums}>
                Godkjenn resultat. !Kan ikke endres!
              </Button.Success>
            </Card>
          ) : (
            ""
          )}
        </>
      );
    } else {
      if (loginDetails.isLoggedIn == true) {
        return (
          <>
            {Alert.warning(
              "Du har ikke tilgang til Oddssiden enda. Kontakt meg for å få tilgang"
            )}
          </>
        );
      } else {
        return (
          <>
            {Alert.warning(
              "Du er ikke logget inn. Gå til forsiden for å gjøre dette"
            )}
          </>
        );
      }
    }
  }
  mounted() {
    betService
      .getOneBet(this.betID)
      .then((bet) => {
        (this.bet = bet), console.log(bet);
      })
      .catch((error: Error) =>
        console.error("Error getting bet: " + error.message)
      );
    betService
      .getOptions(this.betID)
      .then((allOptions) => {
        (this.betOptions = allOptions), console.log(allOptions);
      })
      .catch((error: Error) =>
        console.error("Error getting all options: " + error.message)
      );
  }
  updateAllAccums() {
    console.log("updateAllAccums kjører()");
    this.bet.status = 1;
    betService
      .editBetStatus(1, this.betID)
      .then(() => {
        this.betOptions.forEach((e) => {
          console.log("before wentIn e: " + e);
          if (e.wentIn) {
            console.log("wentIn e: " + e);
            betService
              .editOptionStatus(1, e.optionID)
              .then(() => {})
              .catch((error: Error) =>
                console.error(
                  "Error setting option wentIn to 1: " + error.message
                )
              );
          }
        });
        betService
          .getAllSorted()
          .then((results) => {
            this.allBets = results;
            this.sorted = this.allBets.reduce((r, a, b) => {
              r[a.accumID] = r[a.accumID] || [];
              r[a.accumID].push(a);
              return r;
            }, []);
            console.log(this.sorted);
            this.sorted.forEach((b) => {
              console.log(b);
              b.forEach((w) => {
                if (w.wentIn == 1) {
                  this.accumWentIn = true;
                } else {
                  this.accumHasLost = true;
                }
                if (w.statusBet == 0) {
                  this.isAccumFinished = false;
                }
              });
              if (this.accumWentIn == true && this.accumHasLost == false) {
                console.log("IM in the first if");
                betService
                  .payOut(
                    (b[0].stake * b[0].totalOdds).toFixed(2),
                    b[0].googleID
                  )
                  .then(() => {
                    betService
                      .setPaidOutAndSettled(1, b[0].accumID)
                      .then(() => {
                        betService
                          .incWaitAccums(b[0].googleID)
                          .then(() => {})
                          .catch((error: Error) =>
                            console.error(
                              "Error incrementing waiting accums: " +
                                error.message
                            )
                          );
                      })
                      .catch((error: Error) =>
                        console.error(
                          "Error settiing accum to payed out: " + error.message
                        )
                      );
                  })
                  .catch((error: Error) =>
                    console.error("Error paying out: " + error.message)
                  );
              }
              if (this.accumHasLost == true && this.isAccumFinished == true) {
                console.log("Im here now");
                betService
                  .setSettled(1, b[0].accumID)
                  .then(() => {
                    betService
                      .incWaitAccums(b[0].googleID)
                      .then(() => {})
                      .catch((error: Error) =>
                        console.error(
                          "Error incrementing waiting accums: " + error.message
                        )
                      );
                  })
                  .catch((error: Error) =>
                    console.error(
                      "Error settiing accum to payed out: " + error.message
                    )
                  );
              }
              this.accumWentIn = false;
              this.accumHasLost = false;
              this.isAccumFinished = true;
            });
          })
          .catch((error: Error) =>
            console.error("Error gett bets: " + error.message)
          );
      })
      .catch((error: Error) =>
        console.error("Error setting bet status to 1: " + error.message)
      );
  }

  /*   checkIfPressed(optionID) {
    console.log(optionID);
    console.log(accum.userBets);
    if (accum.userBets.find((e) => e.optionID == optionID) == undefined) {
      console.log('false');
      return false;
    } else {
      return true;
      console.log('true');
    }
  } */
}
