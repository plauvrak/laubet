// @flow
import ReactDOM from 'react-dom';
import * as React from 'react';
import { Component } from 'react-simplified';
import betService, { type Question } from './quiz-service';
import { Card, Row, Column, Form, Button, NavBar } from './widgets';
import { NavLink } from 'react-router-dom';
//import logo from './loading.gif';

import Searchable from 'react-searchable-dropdown';
import { createHashHistory } from 'history';
import { accum } from './index.js';
import { type TypeBet } from './index.js';
const history = createHashHistory();
import { loginDetails } from './home.js';

export class OddsOversikt extends Component {
  userPassword: string = '';
  odds2020: boolean = false;
  odds2021: boolean = true;
  oddsTurn: boolean = false;

  //testOdds: TypeBet[] = [{title: "En av boysa får gnudd seg ila jula", odds: 1.07}, {title: }]

  render() {
    return (
      <>
        <Button.Tabs
          color={this.odds2020}
          onClick={() => {
            if (this.odds2020 == false) {
              this.odds2020 = true;
              this.odds2021 = false;
              this.oddsTurn = false;
            }
          }}
        >
          2020
        </Button.Tabs>
        <Button.Tabs
          color={this.odds2021}
          onClick={() => {
            if (this.odds2021 == false) {
              this.odds2021 = true;
              this.odds2020 = false;
              this.oddsTurn = false;
            }
          }}
        >
          2021
        </Button.Tabs>
        <Button.Tabs
          color={this.oddsTurn}
          onClick={() => {
            if (this.oddsTurn == false) {
              this.oddsTurn = true;
              this.odds2020 = false;
              this.odds2021 = false;
            }
          }}
        >
          Turnering
        </Button.Tabs>

        <Card>
          {this.showOdds2020()}
          {this.showOdds2021()}
          {this.showOddsTurn()}
        </Card>
      </>
    );
  }

  showOdds2020() {
    if (loginDetails.isLoggedIn == true && this.odds2020 == true) {
      return (
        <>
          <div id="parent"></div>

          <Card title="Oddsoversikt - Info">
            Her har dere en foreløpig oddsoversikt. Den er foreløpig svært enkel. <br></br>
            Et langsiktig mål er muligheter for innlogging, virtuelle penger,
            <br></br> sette noen klinke bonger, komme med oddsforslag osv....
            <br></br> Enn så lenge er dette LauBet sine marked, kommer mer utover...
            <br></br>
            <br></br>
            Grunnet litt regler om GDPR og det faktum at jeg faktisk studerer IT-sikkerhet må
            <br></br> jeg være forsiktig med hvilke navn jeg bruker her. Diverse navn kan bli
            <br></br> erstattet av QUIG1 osv.
          </Card>
          <Card title="En av de single boysa får gnudd seg ila jula">
            <Button.Success
              onClick={() => {
                accum.addBet({ title: 'En av de single boysa fpr gnydd seg ila jula', odds: 1.07 });
              }}
            >
              Ja - 1.07
            </Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 8.00</Button.Danger>
          </Card>
          <Card title="Lau gnur seg i jacuzzi igjen">
            <Button.Danger onClick={() => {}}>Ja - 6.00</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.20</Button.Success>
          </Card>
          <Card title="Sim drar fra futsal før det er ferdig for å møte dama">
            <Button.Danger onClick={() => {}}>Ja - 2.40</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.30</Button.Success>
          </Card>
          <Card title="Rich får pult før 2020 er omme">
            <Button.Danger onClick={() => {}}>Ja - 5.50</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.12</Button.Success>
          </Card>
          <Card title="Somdal får pult før 2020 er omme">
            <Button.Success onClick={() => {}}>Ja - 1.70</Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 2.10</Button.Danger>
          </Card>
          <Card title="Somdal får pult før 2020 er omme - flere markeder">
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 1.5 - 3.40</Button.Danger>
                <Button.Success onClick={() => {}}>Under 1.5 - 1.40</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 2.5 - 6.00</Button.Danger>
                <Button.Success onClick={() => {}}>Under 2.5 - 1.20</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 3.5 - 11</Button.Danger>
                <Button.Success onClick={() => {}}>Under 3.5 - 1.10</Button.Success>
              </Column>
            </Row>
          </Card>
          <Card title="Rob får pult før 2020 er omme - VOID MANGLER INFORMASJON">
            <Button.Light onClick={() => {}}>Ja - 2.40</Button.Light>
            <Button.Light onClick={() => {}}>Nei - 1.40</Button.Light>
          </Card>
          <Card title="Lokasjon for Zohan 2018 31.12.2020 kl 22:00">
            <Button.Danger onClick={() => {}}>Hos Julie - 1.40</Button.Danger>
            <Button.Danger onClick={() => {}}>Hytta til Tob - 3.30</Button.Danger>
            <Button.Danger onClick={() => {}}>Hytta til Halv - 3.90</Button.Danger>
            <Button.Danger onClick={() => {}}>Lau - 4.00</Button.Danger>
            <Button.Danger onClick={() => {}}>Annet sted - 4.70</Button.Danger>
            <Button.Success onClick={() => {}}>
              Alle personene er ikke samlet på gitt tidspunkt - 2.10
            </Button.Success>
          </Card>
          <Card title="Thom vender tilbake til gamle flammer">
            <Button.Success onClick={() => {}}>Ja - 1.30</Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 3.50</Button.Danger>
          </Card>
          <Card title="Thom får en 'perfect weekend' i løpet av 2020">
            <Button.Danger onClick={() => {}}>Ja - 3.60</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.20</Button.Success>
          </Card>
          <Card title="Selis får gnudd seg innen 2020 er omme">
            <Button.Danger onClick={() => {}}>Ja - 2.40</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.40</Button.Success>
          </Card>
        </>
      );
    }
  }
  showOdds2021() {
    if (loginDetails.isLoggedIn == true && this.odds2021 == true) {
      return (
        <>
          <div id="parent"></div>

          <Card title="Oddsoversikt - 2021">
            Her har dere en foreløpig oddsoversikt. Den er foreløpig svært enkel. <br></br>
            Et langsiktig mål er muligheter for innlogging, virtuelle penger,
            <br></br> sette noen klinke bonger, komme med oddsforslag osv....
            <br></br> Enn så lenge er dette LauBet sine marked, kommer mer utover...
            <br></br>
            <br></br>
            Grunnet litt regler om GDPR og det faktum at jeg faktisk studerer IT-sikkerhet må
            <br></br> jeg være forsiktig med hvilke navn jeg bruker her. Diverse navn kan bli
            <br></br> erstattet av QUIG1 osv.
          </Card>
          <Card title="En av de single boysa får gnudd seg ila jula">
            <Button.Success
              onClick={() => {
                accum.addBet({ title: 'En av de single boysa fpr gnydd seg ila jula', odds: 1.07 });
              }}
            >
              Ja - 1.07
            </Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 8.00</Button.Danger>
          </Card>
          <Card title="Lau gnur seg i jacuzzi igjen">
            <Button.Danger onClick={() => {}}>Ja - 6.00</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.20</Button.Success>
          </Card>
          <Card title="Sim drar fra futsal før det er ferdig for å møte dama">
            <Button.Danger onClick={() => {}}>Ja - 2.40</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.30</Button.Success>
          </Card>
          <Card title="Rich får pult før 2020 er omme">
            <Button.Danger onClick={() => {}}>Ja - 5.50</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.12</Button.Success>
          </Card>
          <Card title="Somdal får pult før 2020 er omme">
            <Button.Success onClick={() => {}}>Ja - 1.70</Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 2.10</Button.Danger>
          </Card>
          <Card title="Somdal får pult før 2020 er omme - flere markeder">
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 1.5 - 3.40</Button.Danger>
                <Button.Success onClick={() => {}}>Under 1.5 - 1.40</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 2.5 - 6.00</Button.Danger>
                <Button.Success onClick={() => {}}>Under 2.5 - 1.20</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 3.5 - 11</Button.Danger>
                <Button.Success onClick={() => {}}>Under 3.5 - 1.10</Button.Success>
              </Column>
            </Row>
          </Card>
          <Card title="Rob får pult før 2020 er omme - VOID MANGLER INFORMASJON">
            <Button.Light onClick={() => {}}>Ja - 2.40</Button.Light>
            <Button.Light onClick={() => {}}>Nei - 1.40</Button.Light>
          </Card>
          <Card title="Lokasjon for Zohan 2018 31.12.2020 kl 22:00">
            <Button.Danger onClick={() => {}}>Hos Julie - 1.40</Button.Danger>
            <Button.Danger onClick={() => {}}>Hytta til Tob - 3.30</Button.Danger>
            <Button.Danger onClick={() => {}}>Hytta til Halv - 3.90</Button.Danger>
            <Button.Danger onClick={() => {}}>Lau - 4.00</Button.Danger>
            <Button.Danger onClick={() => {}}>Annet sted - 4.70</Button.Danger>
            <Button.Success onClick={() => {}}>
              Alle personene er ikke samlet på gitt tidspunkt - 2.10
            </Button.Success>
          </Card>
          <Card title="Thom vender tilbake til gamle flammer">
            <Button.Success onClick={() => {}}>Ja - 1.30</Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 3.50</Button.Danger>
          </Card>
          <Card title="Thom får en 'perfect weekend' i løpet av 2020">
            <Button.Danger onClick={() => {}}>Ja - 3.60</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.20</Button.Success>
          </Card>
          <Card title="Selis får gnudd seg innen 2020 er omme">
            <Button.Danger onClick={() => {}}>Ja - 2.40</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.40</Button.Success>
          </Card>
        </>
      );
    }
  }
  showOddsTurn() {
    if (loginDetails.isLoggedIn == true && this.oddsTurn == true) {
      return (
        <>
          <div id="parent"></div>

          <Card title="Oversikt over lag">
            <table>
              <Column>
                <Row>
                  <h4>Lag 1</h4>
                </Row>
                <Row>Petter Dømbe</Row>
                <Row>Simen Aanonsen</Row>
                <Row>Sander Lie Seland</Row>
                <Row>Daniel Drivdal</Row>
              </Column>
            </table>
            <br></br>
            <table>
              <Column>
                <Row>
                  <h4>Lag 2</h4>
                </Row>
                <Row>Tor Axel Taasti Bringsverd</Row>
                <Row>Robert Konnestad</Row>
                <Row>Vegard Wilhelmsen</Row>
                <Row>August Asdal Thorsen</Row>
                <Row>Vegard Somdal</Row>
              </Column>
            </table>
            <br></br>
            <table>
              <Column>
                <Row>
                  <h4>Lag 3</h4>
                </Row>
                <Row>Thomas Liene Ness</Row>
                <Row>Mats Johansen</Row>
                <Row>Sondre Grooss Berge</Row>
                <Row>Petter Lauvrak</Row>
              </Column>
            </table>
            <br></br>
            <table>
              <Column>
                <Row>
                  <h4>Lag 4</h4>
                </Row>
                <Row>Markus Svendsen</Row>
                <Row>Petter Ølberg</Row>
                <Row>Morten Svendsen</Row>
                <Row>Haakon Lauvrak</Row>
              </Column>
            </table>
          </Card>
          <Card title="Laget til å løfte trofeet">
            <Button.Success onClick={() => {}}>Lag 1 - 3.10</Button.Success> <br></br>
            <Button.Danger onClick={() => {}}>Lag 2 - 3.60</Button.Danger> <br></br>
            <Button.Danger onClick={() => {}}>Lag 4 - 3.90</Button.Danger> <br></br>
            <Button.Danger onClick={() => {}}>Lag 3 - 4.60</Button.Danger>
          </Card>
          <Card title="Turneringens toppscorer">
            <p>
              <i>
                Pengene tilbake dersom personen ikke møter opp eller spiller under 5 minutter totalt
                i turnering
              </i>
            </p>
            <Button.Danger onClick={() => {}}>Petter Ølberg - 4.80</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Vegard Somdal - 5.00</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Markus Svendsen - 5.30</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Tor Axel Taasti Bringsverd - 5.70</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Petter Lauvrak - 6.00</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Petter Dømbe - 6.30</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Morten Svendsen - 6.70</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Thomas Liene Ness - 7.00</Button.Danger>
            <br></br>
            <Button.Success onClick={() => {}}>Simen Aanonsen - 7.10</Button.Success>
            <br></br>
            <Button.Danger onClick={() => {}}>Sander Lie Seland - 8.50 </Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Vegard Wilhelmsen - 9.80</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Robert Konnestad - 11 </Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Mats Johansen - 15</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Haakon Lauvrak - 15</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>Sondre Grooss Berge - 17</Button.Danger>
            <br></br>
            <Button.Danger onClick={() => {}}>August Asdal Thorsen - 21</Button.Danger>
            <br></br>
          </Card>
          <Card title="Thomas Liene Ness og Vegard Somdal har en intens diskusjon underveis i en kamp?">
            <p>
              <i>Resultatet avgjøres av LauBet sin oddsavdeling</i>
            </p>
            <Button.Danger onClick={() => {}}>Ja - 1.50</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 2.40</Button.Success>
          </Card>
          <Card title="En spiller kommer mer enn 15 minutter for sent til avtalt oppmøte">
            <Button.Success onClick={() => {}}>Ja - 1.20</Button.Success>
            <Button.Danger onClick={() => {}}>Nei - 3.00</Button.Danger>
          </Card>
          <Card title="En spiller blir så skadet/dårlig i form at ambulanse må tilkalles Fevikhallen eller kjøres direkte til Sørlandet Sykehus Arendal">
            <Button.Danger onClick={() => {}}>Ja - 5.70</Button.Danger>
            <Button.Success onClick={() => {}}>Nei - 1.30</Button.Success>
          </Card>
          <Card title="Antall tilskuere i hallen som ikke er deltagere i turneringen når semifinale 1 begynner">
            <Row>
              <Column>
                <Button.Success onClick={() => {}}>Over 0.5 - 1.20</Button.Success>
                <Button.Danger onClick={() => {}}>Under 0.5 - 3.70</Button.Danger>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Success onClick={() => {}}>Over 1.5 - 2.00</Button.Success>
                <Button.Danger onClick={() => {}}>Under 1.5 - 1.90</Button.Danger>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 2.5 - 3.70</Button.Danger>
                <Button.Success onClick={() => {}}>Under 3.5 - 1.20</Button.Success>
              </Column>
            </Row>
          </Card>
          <Card title="Antall enheter mat som blir bestilt fra Happy Time/Pizzabakeren i løpet av turneringen">
            <p>
              <i>En enhet defineres her som én pizza, kebab, tallerken, rull eller lignende</i>
            </p>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 0.5 - 1.20</Button.Danger>
                <Button.Success onClick={() => {}}>Under 0.5 - 3.70</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 1.5 - 1.30</Button.Danger>
                <Button.Success onClick={() => {}}>Under 1.5 - 3.55</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 4.5 - 1.50</Button.Danger>
                <Button.Success onClick={() => {}}>Under 4.5 - 2.40</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 8.5 - 2.00</Button.Danger>
                <Button.Success onClick={() => {}}>Under 8.5 - 1.90</Button.Success>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <Button.Danger onClick={() => {}}>Over 13.5 - 9.00</Button.Danger>
                <Button.Success onClick={() => {}}>Under 13.5 - 1.04</Button.Success>
              </Column>
            </Row>
          </Card>
        </>
      );
    }
  }
}
