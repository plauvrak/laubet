// @flow
import * as React from 'react';
import { Component } from 'react-simplified';
import betService from './quiz-service';
import { Card, Row, Column, Form, Button, NavBar, Alert } from './widgets';
import { createHashHistory } from 'history';
import { loginDetails } from './home.js';
const history = createHashHistory();

export class OrdbokNy extends Component {
  ordbok = [{ ordet: '', innsender: '', hyppighet: 0, beskrivelse: '' }];
  forslag = {
    ordet: '',
    beskrivelse: '',
    hyppighet: null,
    innsender: loginDetails.getName(),
  };
  //reaksjoner = ['ingen', 'ler', 'tommel opp', 'tommel ned'];
  //ord_reaksjoner = [{ ordID: 1, googleID: 123, navn: 'Petter', reaksjon: 1 }];
  render() {
    if (loginDetails.isLoggedIn == true ) {
      return (
        <>
          <div className="OrdbokDiv">
            <Card title="Landvik-ordboka"></Card>
            <Card className="ordbokDiv" title="Send inn forslag her!">
              Ord:{' '}
              <Form.Input
                type="text"
                value={this.forslag.ordet}
                onChange={(event) => {
                  this.forslag.ordet = event.currentTarget.value;
                }}
              ></Form.Input>{' '}
              <br></br>
              Bekskrivelse:{' '}
              <Form.Textarea
                type="text"
                value={this.forslag.beskrivelse}
                onChange={(event) => {
                  this.forslag.beskrivelse = event.currentTarget.value;
                }}
              ></Form.Textarea>
              <Row>
                <Column width={2}> Hyppighet: </Column>
                <Column width={2}>
                  <Form.Input
                    type="number"
                    value={this.forslag.hyppighet}
                    onChange={(event) => {
                      this.forslag.hyppighet = event.currentTarget.value;
                    }}
                  ></Form.Input>
                </Column>
                <Column>/10</Column>
                <Column right>
                  <Button.Success onClick={this.sendForslag}>Send forslag!</Button.Success>
                </Column>
              </Row>
            </Card>
            {this.ordbok.map((ord) => {
              return (
                <Card title={ord.ordet}>
                  <Row>
                    <div className="Bettet">
                      {/*                       {this.checkMyReaction(ord.ordID)}
                       */}
                      <Column>Innsendt av: {ord.innsender}</Column>
                      <Column>Hyppighet: {ord.hyppighet}/10</Column>
                    </div>
                  </Row>
                  <Row>
                    <Card>
                      <Column>{ord.beskrivelse}</Column>
                    </Card>
                  </Row>
                </Card>
              );
            })}
          </div>
        </>
      );
    } else {
      if (loginDetails.isLoggedIn == true) {
        return (
          <>{Alert.warning('Du har ikke tilgang til ordboka enda. Kontakt meg for å få tilgang')}</>
        );
      } else {
        return <>{Alert.warning('Du er ikke logget inn. Gå til forsiden for å gjøre dette')}</>;
      }
    }
  }
  mounted() {
    betService
      .getOrdbok()
      .then((ordbok) => {
        this.ordbok = ordbok;
      })
      .catch((error: Error) => {
        console.error('Could not get ordbok: ' + error.message);
      });
  }
  /* checkMyReaction(ordID: number) {
    this.ord_reaksjoner.map((or) => {
      console.log('her også');
      if (or.googleID == 123) {
        console.log(this.reaksjoner[or.reaksjon]);
        return <Column>{this.reaksjoner[or.reaksjon]}</Column>;
      } else {
        return 'her';
      }
    });
  } */
  sendForslag() {
    if (this.forslag.ordet != '' && this.forslag.beskrivelse != '' && this.forslag.hyppighet != 0) {
      betService
        .sendForslag(
          this.forslag.ordet,
          this.forslag.beskrivelse,
          this.forslag.hyppighet,
          this.forslag.innsender
        )
        .then(() => {})
        .catch((error: Error) => {
          console.error('Could not send forslag: ' + error.message);
        });
      this.forslag.ordet = '';
      this.forslag.hyppighet = null;
      this.forslag.beskrivelse = '';
      Alert.success('Ordet ble sendt inn!');
    } else {
      Alert.warning('Du må fylle ut alle feltene før du kan sende forslag');
    }
  }
}
