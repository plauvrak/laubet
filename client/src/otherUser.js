import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./bet-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";
//export const loginDetails = sharedComponentData({isLoggedIn: false});
export type Bet = {
  accumID: Number,
  theBet: String,
  statusBet: Boolean,
  category: String,
  valg: String,
  wentIn: Boolean,
  odds: Number,
};
export type Accum = {
  accumID: Number,
  totalOdds: Number,
  stake: Number,
};

export class OtherUser extends Component<{
  match: { params: { googleID: number } },
}> {
  responseGoogle = (response) => {
    loginDetails.onLogin(response);
  };
  logout = (response) => {
    loginDetails.onLogout(response);
  };
  userInfo = { googleID: 0, name: "", balance: 0, whitelist: 1 };
  testUser: [] = [];
  bets: Bet[] = [];
  accums: Accum[] = [];
  valuen = "";
  googleIDOfUser = this.props.match.params.googleID;
  render() {
    if (
      loginDetails.googleID == "108164258217260146098" ||
      loginDetails.googleID == "108337724362900289041" ||
      loginDetails.googleID == "102491345022782471996" ||
      loginDetails.googleID == "106991761234830055459"
    ) {
      return (
        <>
          <Card>
            <div className="Name">Kupongene til {this.userInfo.name}</div>
            <div className="Name">GoogleID: {this.googleIDOfUser}</div>
            {/* <div className="Saldo" hidden={loginDetails.isLoggedOut}>
            Saldo: {loginDetails.getBalance()}kr
          </div> */}
            {this.userInfo.whitelist ? (
              <div>
                <Button.Danger
                  onClick={() => {
                    this.changeRights(0, this.googleIDOfUser);
                  }}
                >
                  Fjern fra Whitelist!
                </Button.Danger>
              </div>
            ) : (
              <div>
                <Button.Success
                  onClick={() => {
                    this.changeRights(1, this.googleIDOfUser);
                  }}
                >
                  Legg til i Whitelist!
                </Button.Success>
              </div>
            )}
          </Card>
          <Card>
            {this.accums.map((acc) => {
              return (
                <>
                  <ColorCard color="gainsboro">
                    KupongID: {acc.accumID} <br></br>
                    Totalodds: {acc.totalOdds} <br></br>
                    Innsats: {acc.stake}kr <br></br>
                    Potensiell gevinst: {(acc.stake * acc.totalOdds).toFixed(2)}
                    kr
                    {this.bets.map((bet) => {
                      if (bet.accumID == acc.accumID) {
                        return (
                          <ColorCard
                            color={this.colorOfBet(bet.statusBet, bet.wentIn)}
                          >
                            <div
                              className={this.colorOfText(
                                bet.statusBet,
                                bet.wentIn
                              )}
                            >
                              <Row>
                                <Column>{bet.theBet}</Column>
                              </Row>
                              <Row>
                                <Column>Kategori: {bet.category}</Column>
                              </Row>
                              <Row>
                                <Column>{bet.valg}</Column>
                                <Column right>{bet.odds}</Column>
                              </Row>
                            </div>
                          </ColorCard>
                        );
                      } else {
                        return "";
                      }
                    })}
                  </ColorCard>
                  <br></br>
                </>
              );
            })}
          </Card>
        </>
      );
    } else {
      return (
        <>
          <div hidden={loginDetails.isLoggedIn}>
            <GoogleLogin
              clientId="3720471303-c7oi6s5rg1asp4dei9vaghfhs5etmlfo.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={this.responseGoogle}
              onFailure={this.responseGoogle}
              cookiePolicy={"single_host_origin"}
              isSignedIn={true}
            />
            <br></br>
            <br></br>
          </div>
          <div>
            <img
              src={loginDetails.getImgUrl() ? loginDetails.getImgUrl() : ""}
            />{" "}
            <br></br>
            <div className="Name" hidden={loginDetails.isLoggedOut}>
              {loginDetails.getName()}
            </div>
            <div className="Saldo" hidden={loginDetails.isLoggedOut}>
              Saldo: {loginDetails.getBalance()}kr
            </div>
            <br></br>
          </div>
          <div hidden={loginDetails.isLoggedOut}>
            <GoogleLogout
              clientId="3720471303-c7oi6s5rg1asp4dei9vaghfhs5etmlfo.apps.googleusercontent.com"
              buttonText="Logout"
              onLogoutSuccess={this.logout}
            ></GoogleLogout>
          </div>
        </>
      );
    }
  }
  mounted() {
    betService
      .getBets(this.googleIDOfUser)
      .then((bets) => {
        this.bets = bets;
      })
      .catch((error: Error) =>
        console.error("Error getting bets: " + error.message)
      );
    betService
      .getAccums(this.googleIDOfUser)
      .then((accums) => {
        this.accums = accums;
      })
      .catch((error: Error) =>
        console.error("Error getting accums: " + error.message)
      );
    betService
      .getOneUser(this.googleIDOfUser)
      .then((user) => {
        this.userInfo = user;
      })
      .catch((error: Error) =>
        console.error("Error getting all users: " + error.message)
      );
  }
  colorOfBet(status, wentIn) {
    if (status == 0) {
      /*  if (this.valuen == '' || this.valuen == 'green') {
        this.valuen = 'white';
      } */
      return "white";
    }
    if (wentIn == 1) {
      /* if (this.valuen == 'green' || this.valuen == '') {
        this.valuen = 'green';
      } */
      return "green";
    }
    if (status == 1 && wentIn == 0) {
      /* this.valuen = 'red'; */
      return "red";
    }
  }
  colorOfText(status, wentIn) {
    if (status == 0) {
      /*  if (this.valuen == '' || this.valuen == 'green') {
        this.valuen = 'white';
      } */
      return "";
    }
    if (status == 1 && wentIn == 1) {
      /* if (this.valuen == 'green' || this.valuen == '') {
        this.valuen = 'green';
      } */
      return "WhiteText";
    }
    if (status == 1 && wentIn == 0) {
      /* this.valuen = 'red'; */
      return "WhiteText";
    }
  }
  changeRights(newValue, googleID) {
    if (this.googleIDOfUser != 108164258217260146098) {
      betService
        .changeRights(newValue, googleID)
        .then(() => {
          this.userInfo.whitelist = newValue;
        })
        .catch((error: Error) =>
          console.error("Error getting all users: " + error.message)
        );
    } else {
      Alert.danger("Du kan ikke fjerne Petter Lauvrak fra Whitelist!!");
    }
  }
  /* colorOfAccum(accumID) {
    this.tempArray = this.bets.filter((e) => {
      e.accumID == accumID;
    });

    console.log(this.tempArray);
  } */
}
