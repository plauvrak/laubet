import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./quiz-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";

import Switch from "react-ios-switch";

//export const loginDetails = sharedComponentData({isLoggedIn: false});
export type Bet = {
  accumID: Number,
  theBet: String,
  statusBet: Boolean,
  category: String,
  valg: String,
  wentIn: Boolean,
  odds: Number,
};
export type Accum = {
  accumID: Number,
  totalOdds: Number,
  stake: Number,
  offentlig: Boolean,
};

export class LoginTest extends Component {
  testUser: [] = [];
  bets: Bet[] = [];
  accums: Accum[] = [];
  valuen = "";

  render() {
    return (
      <>
        <Card>
          <img src={loginDetails.getImgUrl() ? loginDetails.getImgUrl() : ""} />{" "}
          <br></br>
          <div className="Name" hidden={loginDetails.isLoggedOut}>
            {loginDetails.getName()}
          </div>
          <div className="Saldo" hidden={loginDetails.isLoggedOut}>
            Saldo: {loginDetails.getBalance()}kr
          </div>
        </Card>
        <Card title="Dine kuponger">
          {this.accums.map((acc, index) => {
            return (
              <>
                <ColorCard color="gainsboro">
                  KupongID: {acc.accumID} <br></br>
                  Totalodds: {acc.totalOdds} <br></br>
                  Innsats: {acc.stake}kr <br></br>
                  Potensiell gevinst: {(acc.stake * acc.totalOdds).toFixed(
                    2
                  )}kr <br></br>
                  Offentlig for alle:{" "}
                  <Switch
                    checked={acc.public ? true : false}
                    onChange={() => {
                      if (acc.public == 1) {
                        acc.public = 0;
                        this.changePublic(acc.accumID, acc.public);
                      } else {
                        acc.public = 1;
                        this.changePublic(acc.accumID, acc.public);
                      }
                    }}
                  />
                  {this.bets.map((bet) => {
                    if (bet.accumID == acc.accumID) {
                      return (
                        <ColorCard
                          color={this.colorOfBet(bet.statusBet, bet.wentIn)}
                        >
                          <div
                            className={this.colorOfText(
                              bet.statusBet,
                              bet.wentIn
                            )}
                          >
                            <Row>
                              <Column>{bet.theBet}</Column>
                            </Row>
                            <Row>
                              <Column>Kategori: {bet.category}</Column>
                            </Row>
                            <Row>
                              <Column>{bet.valg}</Column>
                              <Column right>{bet.odds}</Column>
                            </Row>
                          </div>
                        </ColorCard>
                      );
                    } else {
                      return "";
                    }
                  })}
                </ColorCard>
                <br></br>
              </>
            );
          })}
        </Card>
      </>
    );
  }
  mounted() {
    betService
      .getBets(loginDetails.getId())
      .then((bets) => {
        this.bets = bets;
      })
      .catch((error: Error) =>
        console.error("Error getting bets: " + error.message)
      );
    betService
      .getAccums(loginDetails.getId())
      .then((accums) => {
        (this.accums = accums), console.log(accums);
      })
      .catch((error: Error) =>
        console.error("Error getting accums: " + error.message)
      );
  }
  colorOfBet(status, wentIn) {
    if (status == 0) {
      /*  if (this.valuen == '' || this.valuen == 'green') {
        this.valuen = 'white';
      } */
      return "white";
    }
    if (status == 1 && wentIn == 1) {
      /* if (this.valuen == 'green' || this.valuen == '') {
        this.valuen = 'green';
      } */
      return "green";
    }
    if (status == 1 && wentIn == 0) {
      /* this.valuen = 'red'; */
      return "red";
    }
  }
  colorOfText(status, wentIn) {
    if (status == 0) {
      /*  if (this.valuen == '' || this.valuen == 'green') {
        this.valuen = 'white';
      } */
      return "";
    }
    if (status == 1 && wentIn == 1) {
      /* if (this.valuen == 'green' || this.valuen == '') {
        this.valuen = 'green';
      } */
      return "WhiteText";
    }
    if (status == 1 && wentIn == 0) {
      /* this.valuen = 'red'; */
      return "WhiteText";
    }
  }
  changePublic(accumID, offentlig) {
    betService
      .changePublic(accumID, offentlig)
      .then(() => {})
      .catch((error: Error) =>
        console.error("Error changing public value: " + error.message)
      );
  }
  /* colorOfAccum(accumID) {
    this.tempArray = this.bets.filter((e) => {
      e.accumID == accumID;
    });

    console.log(this.tempArray);
  } */
}
