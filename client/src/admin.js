import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./bet-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";
import { NavLink } from "react-router-dom";

export type Alternative = {
  option_text: String,
  odds: Number,
  betID: Number,
};

export class Admin extends Component {
  responseGoogle = (response) => {
    loginDetails.onLogin(response);
  };
  logout = (response) => {
    loginDetails.onLogout(response);
  };
  bet: [] = [{ theBet: "", category: "" }];
  allUsersInfo: [] = [];
  allBets: [] = [];

  alternatives: Alternative[] = [{ option_text: "", odds: null, betID: 0 }];
  render() {
    if (
      loginDetails.googleID == "108164258217260146098" ||
      loginDetails.googleID == "108337724362900289041" ||
      loginDetails.googleID == "102491345022782471996" ||
      loginDetails.googleID == "106991761234830055459"
    ) {
      return (
        <>
          <div>
            <table id="customers">
              <tbody>
                <tr>
                  <td>Navn</td>
                  <td>Saldo</td>
                  <td>Whitelisted</td>
                  <td>Last Login</td>
                </tr>
                {this.allUsersInfo.map((user) => {
                  return (
                    <tr>
                      <td>
                        <div>
                          <NavLink to={"admin/bets/" + user.googleID}>
                            {user.name}
                          </NavLink>
                        </div>
                      </td>
                      <td>{user.balance}</td>
                      <td>{user.whitelist ? "✅" : "❌"}</td>
                      <td>
                        {user.lastLogin
                          ? user.lastLogin
                              .toString()
                              .replace("T", " ")
                              .replace("Z", " ")
                          : "N/A"}
                      </td>
                    </tr>
                  );
                })}
              </tbody>
            </table>
            <Card title="Legg til nytt bet:">
              <Form.Input
                placeholder="Bet"
                value={this.bet[0].theBet}
                onChange={(event) => {
                  this.bet[0].theBet = event.currentTarget.value;
                }}
              ></Form.Input>
              <Form.Input
                placeholder="Kategori"
                value={this.bet[0].category}
                onChange={(event) => {
                  this.bet[0].category = event.currentTarget.value;
                }}
              ></Form.Input>
              <br></br>
              {this.alternatives.map((alternative, index) => {
                return (
                  <>
                    <Row>
                      <Column>
                        <Form.Input
                          placeholder={"Alternativ " + (index + 1)}
                          value={alternative.option_text}
                          onChange={(event) => {
                            alternative.option_text = event.currentTarget.value;
                          }}
                        ></Form.Input>
                      </Column>
                      <Column right>
                        <Form.Input
                          placeholder="Odds"
                          value={alternative.odds}
                          onChange={(event) => {
                            alternative.odds = event.currentTarget.value;
                          }}
                        ></Form.Input>
                      </Column>
                    </Row>
                    <br></br>
                  </>
                );
              })}
              <Row>
                <Column>
                  <Button.Light
                    onClick={() => {
                      this.alternatives.push({
                        option_text: "",
                        odds: null,
                        betID: 0,
                      });
                    }}
                  >
                    Legg til alternativ
                  </Button.Light>{" "}
                </Column>
                <Column right>
                  <Button.Success
                    onClick={() => {
                      this.sendBet(this.bet, this.alternatives);
                      this.bet = [{ theBet: "", category: "" }];
                      this.alternatives = [
                        { option_text: "", odds: null, betID: 0 },
                      ];
                      //this.altivernatives[odds] = null;
                    }}
                  >
                    Legg til bet!
                  </Button.Success>
                </Column>{" "}
              </Row>
            </Card>
            <Card title="Aktive bets:">
              {this.allBets.map((bet) => {
                if (bet.status == 0) {
                  return (
                    <Row key={bet.betID}>
                      <Column>
                        <NavLink to={"/admin/" + bet.betID}>{bet.name}</NavLink>
                      </Column>
                    </Row>
                  );
                }
              })}
            </Card>
            <Card title="Ferdige bets:">
              {this.allBets.map((bet) => {
                if (bet.status == 1) {
                  return (
                    <Row key={bet.betID}>
                      <Column>
                        <NavLink to={"/admin/" + bet.betID}>{bet.name}</NavLink>
                      </Column>
                    </Row>
                  );
                }
              })}
            </Card>
          </div>
        </>
      );
    } else {
      if (loginDetails.isLoggedIn == true) {
        return (
          <>
            {Alert.warning(
              "Du har dessverre ikke tilgang til admin-siden... På denne siden vil man kunne legge til odds, markere spill som ferdige osv. Kanskje jeg trenger noen medhjelpere etterhvert... Bare å melde seg til admin!"
            )}
          </>
        );
      } else {
        return (
          <>
            <div hidden={loginDetails.isLoggedIn}>
              <GoogleLogin
                clientId="3720471303-c7oi6s5rg1asp4dei9vaghfhs5etmlfo.apps.googleusercontent.com"
                buttonText="Login"
                onSuccess={this.responseGoogle}
                onFailure={this.responseGoogle}
                cookiePolicy={"single_host_origin"}
                isSignedIn={true}
              />
              <br></br>
              <br></br>
            </div>
            <div>
              <img
                src={loginDetails.getImgUrl() ? loginDetails.getImgUrl() : ""}
              />{" "}
              <br></br>
              <div className="Name" hidden={loginDetails.isLoggedOut}>
                {loginDetails.getName()}
              </div>
              <div className="Saldo" hidden={loginDetails.isLoggedOut}>
                Saldo: {loginDetails.getBalance()}kr
              </div>
              <br></br>
            </div>
            <div hidden={loginDetails.isLoggedOut}>
              <GoogleLogout
                clientId="3720471303-6ml6ek54ak64t31qfa1v94r751df1j4b.apps.googleusercontent.com"
                buttonText="Logout"
                onLogoutSuccess={this.logout}
              ></GoogleLogout>
            </div>
          </>
        );
      }
    }
  }
  sendBet(bet, alternatives) {
    betService
      .sendBet(bet[0].theBet, bet[0].category)
      .then((newBetID) => {
        alternatives.forEach((alt) => {
          alt.betID = newBetID.betID;
          betService
            .sendAlts(alt.option_text, alt.odds, alt.betID)
            .then(() => {})
            .catch((error: Error) =>
              console.error("Error sending alternative: " + error.message)
            );
        });
      })
      .catch((error: Error) =>
        console.error("Error sending bet: " + error.message)
      );
  }
  mounted() {
    betService
      .getAllUsers()
      .then((allUsers) => {
        this.allUsersInfo = allUsers;
      })
      .catch((error: Error) =>
        console.error("Error getting all users: " + error.message)
      );
    betService
      .getDefAllBets()
      .then((allBets) => {
        this.allBets = allBets;
      })
      .catch((error: Error) =>
        console.error("Error getting all bets: " + error.message)
      );
  }
}
