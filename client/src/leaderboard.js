import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./quiz-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";
import { NavLink } from "react-router-dom";

export class Leaderboard extends Component {
  allUsersInfo: [] = [];
  render() {
    if (loginDetails.isLoggedIn == true) {
      return (
        <>
          <div>
            <Card>
              <table id="customers">
                <tbody>
                  <tr>
                    <td>Navn</td>
                    <td>Saldo</td>
                  </tr>
                  {this.allUsersInfo.map((user) => {
                    if (user.whitelist == 1)
                      return (
                        <tr>
                          <td>
                            <div>
                              <NavLink
                                to={
                                  "leaderboard/" +
                                  user.googleID +
                                  "/" +
                                  user.name
                                }
                              >
                                {user.name}
                              </NavLink>
                            </div>
                          </td>
                          <td>{user.balance}</td>
                        </tr>
                      );
                  })}
                </tbody>
              </table>
            </Card>
          </div>
        </>
      );
    } else {
      return (
        <>
          <Card>Du er ikke logget inn</Card>
        </>
      );
    }
  }
  mounted() {
    betService
      .getAllUsers()
      .then((allUsers) => {
        this.allUsersInfo = allUsers;
      })
      .catch((error: Error) =>
        console.error("Error getting all users: " + error.message)
      );
  }
}
