// @flow
import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type Question } from "./bet-service";
import {
  Card,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  CenterCard,
  ColorCard,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";

import { TwitterTweetEmbed } from "react-twitter-embed";

import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";

class LoginDetails {
  allUsers: [] = [];
  userExists = false;
  googleID: number = 0;
  isLoggedIn: boolean = false;
  isLoggedOut: boolean = true;
  googleImgUrl: string = "";
  balance: number = 0;
  googleName: string = "";
  whitelisted: boolean = false;

  onLogin(response) {
    this.googleImgUrl = response.profileObj.imageUrl;
    this.googleID = response.profileObj.googleId;
    this.isLoggedIn = true;
    this.isLoggedOut = false;
    this.googleName = response.profileObj.name;
    betService
      .getAllUserIDs()
      .then((allUsers) => {
        this.allUsers = allUsers;
        this.allUsers.forEach((e) => {
          if (e.googleID == this.googleID) {
            betService
              .getOneUser(this.googleID)
              .then((balance) => {
                this.balance = balance.balance;
                this.whitelisted = balance.whitelist;
                betService
                  .loginStamp(this.googleID)
                  .then(() => {})
                  .catch((error: Error) =>
                    console.error(
                      "Error reseting waiting accums: " + error.message
                    )
                  );
                if (balance.waitingAccums) {
                  Alert.info(
                    "Du har " +
                      balance.waitingAccums +
                      " spill klare til sjekk!"
                  );
                  betService
                    .noWaiting(this.googleID)
                    .then(() => {})
                    .catch((error: Error) =>
                      console.error(
                        "Error reseting waiting accums: " + error.message
                      )
                    );
                }
              })
              .catch((error: Error) =>
                console.error("Error getting accums: " + error.message)
              );
            this.userExists = true;
          }
        });
        if (!this.userExists) {
          betService
            .addUser(this.googleID, this.googleName)
            .then(() => {
              betService
                .getOneUser(this.googleID)
                .then((balance) => {
                  this.balance = balance.balance;
                  this.whitelisted = balance.whitelist;
                  betService
                    .loginStamp(this.googleID)
                    .then(() => {})
                    .catch((error: Error) =>
                      console.error(
                        "Error reseting waiting accums: " + error.message
                      )
                    );
                  Alert.info(
                    "Du har ikke tilgang til noen av sidene enda. Kontakt meg for å få tilgang"
                  );
                })
                .catch((error: Error) =>
                  console.error("Error getting balance: " + error.message)
                );
            })
            .catch((error: Error) =>
              console.error("Error adding user: " + error.message)
            );
        }
      })
      .catch((error: Error) =>
        console.error("Error getting all bets: " + error.message)
      );
  }
  onLogout(response) {
    this.googleImgUrl = "";
    this.googleID = 0;
    loginDetails.balance = 0;
    this.isLoggedIn = false;
    document.location.reload(true);
  }
  getId() {
    return this.googleID;
  }
  getImgUrl() {
    if (this.googleImgUrl) {
      return this.googleImgUrl;
    } else {
      return "";
    }
  }
  getName() {
    return this.googleName;
  }
  updateBalance(amount: number) {
    this.balance += amount;
  }
  getBalance() {
    return this.balance;
  }
  setAllUserIDs(allIDs: []) {
    this.allUsers = allIDs;
  }
}
export const loginDetails = sharedComponentData(new LoginDetails());

export class Home extends Component {
  imgUrl: string = "";
  responseGoogle = (response) => {
    loginDetails.onLogin(response);
  };
  responseFailed = (response) => {};
  logout = (response) => {
    loginDetails.onLogout(response);
  };
  render() {
    return (
      <div class="text-center">
        <Card>
          <h5>🎄Hjertelig velkommen til🎄</h5>
          <br></br>
          <br></br>
          <img
            src="/Media/logo3_jul.png"
            height="150"
            className="Forsidebilde"
          ></img>
          <div hidden={loginDetails.isLoggedIn}>
            <GoogleLogin
              clientId="3720471303-6ml6ek54ak64t31qfa1v94r751df1j4b.apps.googleusercontent.com"
              buttonText="Login"
              onSuccess={this.responseGoogle}
              onFailure={this.responseFailed}
              cookiePolicy={"single_host_origin"}
              isSignedIn={true}
            />
            <br></br>
            <br></br>
          </div>
          <div>
            <img
              src={loginDetails.getImgUrl() ? loginDetails.getImgUrl() : ""}
            />{" "}
            <br></br>
            <div className="Name" hidden={loginDetails.isLoggedOut}>
              {loginDetails.getName()}
            </div>
            <div className="Saldo" hidden={loginDetails.isLoggedOut}>
              Saldo: {loginDetails.getBalance()}kr
            </div>
            <br></br>
          </div>
          <div hidden={loginDetails.isLoggedOut}>
            <GoogleLogout
              clientId="3720471303-6ml6ek54ak64t31qfa1v94r751df1j4b.apps.googleusercontent.com"
              buttonText="Logout"
              onLogoutSuccess={this.logout}
            ></GoogleLogout>
          </div>
          <br></br>
          <div className="Forsidetekst">
            <h5>Ordboka - trenger påfyll!</h5>
            Ordboka er oppdatert med masse nye ord og også mulighet for alle til
            å legge inn nye ord selv! Sjekk den ut!! <br></br>
            <Button.Lau center onClick={this.sendOrdbok}>
              🔎 Gå til ordboka!
            </Button.Lau>
          </div>
          <br></br>
          <h5>REQUEST-A-BET!</h5>
          Nå kan alle boysa foreslå odds som skal inn på LauBet! Du velger selv
          om du vil legge inn spill-alternativer med odds eller om jeg skal
          gjøre det for deg. Hvis du ikke ønsker å legge inn spill-alternativer,
          lar du bare dette feltet stå tomt når du sender inn.
          <br></br>
          Alle kan se hva de andre har meldt inn, så her er det bare å melde i
          vei🔥
          <br></br>
          <Button.Lau center onClick={this.sendToReq}>
            🔎 Gå til request-a-bet!
          </Button.Lau>
          <br></br>
          <br></br>
          <Button.Lau center onClick={this.send}>
            🔎 Gå til odds-siden!
          </Button.Lau>
          <br></br>
        </Card>
        <br></br>
        <br></br>
      </div>
    );
  }

  mounted() {}

  send() {
    history.push("/oddsny");
  }
  sendToReq() {
    history.push("/request");
  }
  sendOrdbok() {
    history.push("/ordbokNy");
  }
}
