import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./quiz-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";
import { type Alternative } from "./admin.js";

export class RequestBet extends Component {
  bet: [] = [{ theBet: "", category: "" }];
  alternatives: Alternative[] = [{ option_text: "", odds: null, betID: 0 }];
  allBets: [] = [];
  allOpts: [] = [];

  render() {
    if (loginDetails.isLoggedIn == true) {
      return (
        <>
          <div>
            <Card title="Foreslå et nytt bet:">
              <Form.Input
                placeholder="Bet"
                value={this.bet[0].theBet}
                onChange={(event) => {
                  this.bet[0].theBet = event.currentTarget.value;
                }}
              ></Form.Input>
              <Form.Input
                placeholder="Kategori"
                value={this.bet[0].category}
                onChange={(event) => {
                  this.bet[0].category = event.currentTarget.value;
                }}
              ></Form.Input>
              <br></br>
              {this.alternatives.map((alternative, index) => {
                return (
                  <>
                    <Row>
                      <Column>
                        <Form.Input
                          placeholder={"Alternativ " + (index + 1)}
                          value={alternative.option_text}
                          onChange={(event) => {
                            alternative.option_text = event.currentTarget.value;
                          }}
                        ></Form.Input>
                      </Column>
                      <Column right>
                        <Form.Input
                          placeholder="Odds"
                          value={alternative.odds}
                          onChange={(event) => {
                            alternative.odds = event.currentTarget.value;
                          }}
                        ></Form.Input>
                      </Column>
                    </Row>
                    <br></br>
                  </>
                );
              })}
              <Row>
                <Column>
                  <Button.Light
                    onClick={() => {
                      this.alternatives.push({
                        option_text: "",
                        odds: null,
                        betID: 0,
                      });
                    }}
                  >
                    Legg til alternativ
                  </Button.Light>{" "}
                </Column>
                <Column right>
                  <Button.Success
                    onClick={() => {
                      this.sendReq(this.bet, this.alternatives);
                      this.bet = [{ theBet: "", category: "" }];
                      this.alternatives = [
                        { option_text: "", odds: null, betID: 0 },
                      ];
                      this.altivernatives[odds] = null;
                    }}
                  >
                    Legg til bet!
                  </Button.Success>
                </Column>{" "}
              </Row>
              <div>
                <br></br>
                <br></br>
                <br></br>
                <h1>INNSENDTE BETS:</h1>
                {this.allBets
                  .slice(0)
                  .reverse()
                  .map((bet) => {
                    return (
                      <>
                        <Card title={bet.name}>
                          Innsendt av: {bet.requestedBy} <br></br>
                          Kategori: {bet.category}
                          <div>
                            {this.allOpts.map((opt) => {
                              if (opt.betID == bet.betID) {
                                return (
                                  <>
                                    <Button.Light onClick={() => {}}>
                                      {opt.option_text} {opt.odds}
                                    </Button.Light>
                                  </>
                                );
                              }
                            })}
                          </div>
                        </Card>
                      </>
                    );
                  })}
              </div>
            </Card>
          </div>
        </>
      );
    } else {
      return (
        <>
          <Card>Du er ikke logget inn</Card>
        </>
      );
    }
  }
  mounted() {
    betService
      .getAllBetsReq()
      .then((allBets) => {
        this.allBets = allBets;
      })
      .catch((error: Error) =>
        console.error("Error getting all bets: " + error.message)
      );
    betService
      .getAllReqOpts()
      .then((allOpts) => {
        this.allOpts = allOpts;
      })
      .catch((error: Error) =>
        console.error("Error getting all opts: " + error.message)
      );
  }
  sendReq(bet, alternatives) {
    betService
      .sendReq(bet[0].theBet, bet[0].category, loginDetails.getName())
      .then((newBetID) => {
        alternatives.forEach((alt) => {
          alt.betID = newBetID.betID;
          betService
            .sendReqAlts(alt.option_text, alt.odds, alt.betID)
            .then(() => {
              betService
                .getAllBetsReq()
                .then((allBets) => {
                  this.allBets = allBets;
                })
                .catch((error: Error) =>
                  console.error("Error getting all bets: " + error.message)
                );
              betService
                .getAllReqOpts()
                .then((allOpts) => {
                  this.allOpts = allOpts;
                })
                .catch((error: Error) =>
                  console.error("Error getting all opts: " + error.message)
                );
            })
            .catch((error: Error) =>
              console.error("Error sending alternative: " + error.message)
            );
        });
      })
      .catch((error: Error) =>
        console.error("Error sending bet: " + error.message)
      );
  }
}
