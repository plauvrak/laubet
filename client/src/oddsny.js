import * as React from "react";
import { Component, sharedComponentData } from "react-simplified";
import betService, { type User } from "./quiz-service";
import {
  Card,
  ColorCard,
  Row,
  Column,
  Form,
  Button,
  NavBar,
  Alert,
} from "./widgets";
import { createHashHistory } from "history";
import { GoogleLogin } from "react-google-login";
const history = createHashHistory();
import { GoogleLogout } from "react-google-login";
import { loginDetails } from "./home.js";

export type UserAccum = {
  betID: Number,
  theBet: string,
  optionID: Number,
  valg: String,
  odds: number,
  keyToBetID: Number,
};

export class BetSlip extends Component {
  render() {
    return (
      <>
        {accum.makeAccumulator()}
        {accum.plassertSpill()}
      </>
    );
  }
}
class Accumulator {
  userBets: UserAccum[] = [];
  empty: boolean = true;
  isHidden: boolean = false;
  innsats: number = 0;
  totalOdds: number = 1;
  indexOfOption: number = null;
  createdAccumID: number = null;
  brukerLukket: boolean = true;

  makeAccumulator() {
    if (this.empty) return null;
    else {
      return (
        <div className="Accum">
          <Button.Light
            center
            onClick={() => {
              if (this.isHidden == true) {
                this.isHidden = false;
              } else {
                this.isHidden = true;
              }
            }}
          >
            {this.isHidden ? "Vis kupong" : "Skjul kupong"}
          </Button.Light>
          <div hidden={this.isHidden}>
            <br></br>

            {this.userBets.map((bet, index) => {
              return (
                <Row>
                  <Column width={"10%"}>
                    <Button.Danger
                      onClick={() => {
                        this.userBets.splice(index, 1);

                        this.calcTotalOdds();
                        if (this.userBets[0] == undefined) {
                          this.empty = true;
                          this.innsats = 0;
                        }
                      }}
                    >
                      X
                    </Button.Danger>
                  </Column>
                  <Column>
                    <Row>
                      <Column>
                        <div className="Bettet">{bet.theBet}</div>
                      </Column>
                    </Row>
                    <Row>
                      <Column>
                        <div className="Valget">{bet.valg}</div>
                      </Column>
                      <Column className="Valget" right width={"10%"}>
                        {bet.odds}
                      </Column>
                    </Row>
                  </Column>
                </Row>
              );
            })}
            <br></br>
            <Row>
              <Column>
                <Form.Input
                  inputProps={{ inputMode: "numeric" }}
                  placeholder="Innsats"
                  type="number"
                  value={this.innsats}
                  onChange={(event) => {
                    this.innsats = event.currentTarget.value;
                  }}
                ></Form.Input>
              </Column>
              <Column right>
                <div className="Valget">Totalodds: {this.totalOdds}</div>
              </Column>
            </Row>
            <br></br>
            <Row>
              <Column>
                <div className="Bettet">
                  Mulig gevinst: {(this.innsats * this.totalOdds).toFixed(2)}kr
                </div>
              </Column>
              <Column right>
                <Button.Success
                  onClick={() => {
                    this.placeBet();
                  }}
                >
                  Plassér spill
                </Button.Success>
              </Column>
            </Row>
          </div>
        </div>
      );
    }
  }

  placeBet() {
    if (
      loginDetails.googleID != 0 &&
      this.innsats > 0 &&
      loginDetails.balance >= this.innsats
    ) {
      betService
        .makeAccum(loginDetails.googleID, this.totalOdds, this.innsats)
        .then((accumID) => {
          loginDetails.balance -= this.innsats;
          this.createdAccumID = accumID.accumID;
          betService
            .updateBalance(loginDetails.googleID, loginDetails.balance)
            .then(() => {})
            .catch((error: Error) =>
              console.error("Error placing the bets: " + error.message)
            );
          this.userBets.forEach((u) => {
            betService
              .placeBet(this.createdAccumID, u.betID, u.optionID)
              .then(() => {})
              .catch((error: Error) =>
                console.error("Error placing the bets: " + error.message)
              );
          });
          this.totalOdds = 1;
          this.innsats = 0;
          this.userBets = [];
          this.empty = true;
          this.brukerLukket = false;
          this.plassertSpill();
        })
        .catch((error: Error) =>
          console.error("Error getting all bets: " + error.message)
        );
    } else {
      console.log("noe galt skjedde");
    }
  }

  plassertSpill() {
    if (this.brukerLukket) {
      return "";
    } else {
      return (
        <div className="Accum2">
          <div className="SpillPlassert">Spillet er plassert!</div>
          <Button.Light
            center
            onClick={() => {
              this.brukerLukket = true;
            }}
          >
            Lukk
          </Button.Light>
          <Button.Lau
            onClick={() => {
              this.brukerLukket = true;
              history.push("/loginTest");
            }}
          >
            Gå til Mine Spill
          </Button.Lau>
        </div>
      );
    }
  }

  addBet(bet: UserAccum) {
    this.empty = false;
    if (this.userBets.find((e) => e.optionID == bet.optionID) != undefined) {
      this.indexOfOption = this.userBets.findIndex(
        (e) => e.optionID == bet.optionID
      );
      this.userBets.splice(this.indexOfOption, 1);
      if (this.userBets[0] == undefined) {
        this.empty = true;
        this.innsats = 0;
      }
    } else {
      this.userBets.push(bet);
    }

    this.calcTotalOdds();
  }
  removeBet(bet: TypeBet) {}

  calcTotalOdds() {
    this.totalOdds = 1;
    this.userBets.forEach((k) => {
      this.totalOdds = (this.totalOdds * k.odds).toFixed(2);
    });
  }
}

export const accum = sharedComponentData(new Accumulator());

export type ABet = {
  betID: Number,
  theBet: string,
  category: string,
};
export type Option = {
  optionID: Number,
  valg: string,
  odds: Number,
  keyToBetID: Number,
};

export class OddsNy extends Component {
  bets: ABet[] = [];
  betOptions: Option[] = [];

  render() {
    if (loginDetails.isLoggedIn == true && loginDetails.whitelisted == true) {
      return (
        <>
          <Card>Saldo: {loginDetails.getBalance()}kr</Card>
          {this.bets.map((bet) => {
            return (
              <Card title={bet.name}>
                Kategori: {bet.category} <br></br>
                {this.betOptions.map((option) => {
                  if (option.keyToBetID == bet.betID) {
                    return (
                      <Button.Odds
                        pressed={this.checkIfPressed(option.optionID)}
                        onClick={() => {
                          accum.addBet({
                            betID: bet.betID,
                            theBet: bet.name,
                            optionID: option.optionID,
                            valg: option.valg,
                            odds: option.odds,
                            keyToBetID: option.keyToBetID,
                          });
                        }}
                      >
                        {option.valg} - {option.odds.toFixed(2)}
                      </Button.Odds>
                    );
                  } else {
                    return "";
                  }
                })}
              </Card>
            );
          })}
        </>
      );
    } else {
      if (loginDetails.isLoggedIn == true) {
        return (
          <>
            {Alert.warning(
              "Du har ikke tilgang til Oddssiden enda. Kontakt meg for å få tilgang"
            )}
          </>
        );
      } else {
        return (
          <>
            {Alert.warning(
              "Du er ikke logget inn. Gå til forsiden for å gjøre dette"
            )}
          </>
        );
      }
    }
  }
  mounted() {
    betService
      .getAllBets()
      .then((bets) => {
        this.bets = bets;
      })
      .catch((error: Error) =>
        console.error("Error getting all bets: " + error.message)
      );
    betService
      .getAllOptions()
      .then((allOptions) => {
        this.betOptions = allOptions;
      })
      .catch((error: Error) =>
        console.error("Error getting all options: " + error.message)
      );
  }
  checkIfPressed(optionID) {
    if (accum.userBets.find((e) => e.optionID == optionID) == undefined) {
      return false;
    } else {
      return true;
    }
  }
}
