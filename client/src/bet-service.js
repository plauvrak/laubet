// @flow
import axios from "axios";
import { type Bet } from "./logintest.js";
import { type Accum } from "./logintest.js";
import { type ABet } from "./oddsny.js";
import { type Option } from "./oddsny.js";

axios.defaults.baseURL = "/api/v1";

export type User = {
  googleID: number,
  name: string,
  balance: number,
  pictureID: string,
};

class BetService {

  //hente alle bets til en bruker
  getBets(googleID: number) {
    return axios
      .get<Bet[]>("/profile/getBets/" + googleID)
      .then((response) => response.data);
  }

  //hente alle kuponger til en bruker
  getAccums(googleID: number) {
    return axios
      .get<Accum[]>("/profile/getAccums/" + googleID)
      .then((response) => response.data);
  }
  //hente saldot il en bruker
  getBalanceAndWL(googleID: number) {
    return axios
      .get<any>("/profile/getBalance/" + googleID)
      .then((response) => response.data);
  }
  //hente alle bets i hele databasen
  getAllBets() {
    return axios.get<ABet[]>("/odds/getAll").then((response) => response.data);
  }
  //hente alle bet options i hele databasen
  getAllOptions() {
    return axios
      .get<Option[]>("/odds/getAllOptions")
      .then((response) => response.data);
  }
  //hente ett spesifikt bet
  getOneBet(betID: number) {
    return axios
      .get<any>("/admin/getOneBet/" + betID)
      .then((response) => response.data);
  }
  //hente alle options til ett spesifikt bet
  getOptions(betID: number) {
    return axios
      .get<any>("/admin/getOptions/" + betID)
      .then((response) => response.data);
  }

  makeAccum(googleID: Number, totalOdds: Number, innsats: Number) {
    return axios
      .post("/odds/makeAccum", {
        googleID: googleID,
        totalOdds: totalOdds,
        innsats: innsats,
      })
      .then((response) => response.data);
  }
  placeBet(accumID: number, betID: number, optionID: number) {
    return axios
      .post("/odds/placeBet", {
        accumID: accumID,
        betID: betID,
        optionID: optionID,
      })
      .then((response) => response.data);
  }
  changePublic(accumID: number, offentlig: boolean) {
    return axios
      .put("/profile/changePublic", {
        accumID: accumID,
        offentlig: offentlig,
      })
      .then((response) => response.data);
  }

  getAllUserIDs() {
    return axios.get("/home/allUsers").then((response) => response.data);
  }
  getAllSorted() {
    return axios.get("/admin/getAllSorted").then((response) => response.data);
  }
  getDefAllBets() {
    return axios.get("/admin/getAllBets").then((response) => response.data);
  }
  getAllBetsReq() {
    return axios.get("/admin/getAllReqs").then((response) => response.data);
  }
  getAllReqOpts() {
    return axios.get("/admin/getAllReqOpts").then((response) => response.data);
  }
  getAllUsers() {
    return axios.get("/admin/allUsers").then((response) => response.data);
  }
  getOneUser(googleID: number) {
    return axios
      .get("/admin/oneUser/" + googleID)
      .then((response) => response.data);
  }
  getOrdbok() {
    return axios.get("/ordbok/getHele").then((response) => response.data);
  }
  changeRights(newValue: number, googleID: number) {
    return axios
      .put("/admin/changeRights/", { newValue: newValue, googleID: googleID })
      .then((response) => response.data);
  }
  editBetText(newText: string, betID: number) {
    return axios
      .put("/admin/editBetText/", { newText: newText, betID: betID })
      .then((response) => response.data);
  }
  editBetStatus(status: number, betID: number) {
    return axios
      .put("/admin/editBetStatus/", { status: status, betID: betID })
      .then((response) => response.data);
  }
  editOptionText(newText: string, odds: number, optionID: number) {
    return axios
      .put("/admin/editOptionText/", {
        newText: newText,
        odds: odds,
        optionID: optionID,
      })
      .then((response) => response.data);
  }
  incWaitAccums(googleID: number) {
    return axios
      .put("/admin/incWaitAccums/", { googleID: googleID })
      .then((response) => response.data);
  }
  editOptionStatus(status: number, optionID: number) {
    return axios
      .put("/admin/editOptionStatus/", { status: status, optionID: optionID })
      .then((response) => response.data);
  }
  addUser(googleID: number, googleName: string) {
    return axios
      .post("/home/addUser", { googleID: googleID, googleName: googleName })
      .then((response) => response.data);
  }
  sendBet(theBet: string, category: string) {
    return axios
      .post("/admin/sendBet", { theBet: theBet, category: category })
      .then((response) => response.data);
  }
  sendReq(theBet: string, category: string, name: string) {
    return axios
      .post("/admin/sendReq", {
        theBet: theBet,
        category: category,
        name: name,
      })
      .then((response) => response.data);
  }
  sendForslag(
    ordet: string,
    beskrivelse: string,
    hyppighet: number,
    innsender: string
  ) {
    return axios
      .post("/ordbok/sendForslag", {
        ordet: ordet,
        beskrivelse: beskrivelse,
        hyppighet: hyppighet,
        innsender: innsender,
      })
      .then((response) => response.data);
  }
  sendAlts(option_text: string, odds: number, betID: number) {
    return axios
      .post("/admin/sendAlt", {
        option_text: option_text,
        odds: odds,
        betID: betID,
      })
      .then((response) => response.data);
  }
  sendReqAlts(option_text: string, odds: number, betID: number) {
    return axios
      .post("/admin/sendReqAlt", {
        option_text: option_text,
        odds: odds,
        betID: betID,
      })
      .then((response) => response.data);
  }

  updateBalance(googleID: number, innsats: number) {
    return axios
      .put("/odds/updateBalance", { googleID: googleID, innsats: innsats })
      .then((response) => response.data);
  }
  payOut(amount: number, googleID: number) {
    return axios
      .put("/admin/payOut", { amount: amount, googleID: googleID })
      .then((response) => response.data);
  }
  setPaidOutAndSettled(setValue: number, accumID: number) {
    return axios
      .put("/admin/setPaidOut", { setValue: setValue, accumID: accumID })
      .then((response) => response.data);
  }
  setSettled(setValue: number, accumID: number) {
    return axios
      .put("/admin/setSettled", { setValue: setValue, accumID: accumID })
      .then((response) => response.data);
  }
  noWaiting(googleID: number) {
    return axios
      .put("/home/noWaiting", { googleID: googleID })
      .then((response) => response.data);
  }
  loginStamp(googleID: number) {
    return axios
      .put("/home/loginStamp", { googleID: googleID })
      .then((response) => response.data);
  }
}
const betService = new BetService();
export default betService;

